<?php

$json_url = 'http://localhost:3000';
$front_url = 'http://localhost:9000';
$payment_url = 'http://localhost:8888';

$username = 'nooxr';  // authentication
$password = '123123toi$';  // authentication

// jSON String for request
$json_string = '/v1/payment/guide/'.$_GET["guide"];

if(!$_GET["guide"] || $_GET["guide"] == '' ){
  header('Location: '.$front_url);
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $json_url.$json_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
$output = curl_exec($ch);
$info = curl_getinfo($ch);
curl_close($ch);

$invoice = json_decode($output, true);

if ($invoice['error'] == "1" || !$invoice['guide_number']){
  header('Location: '.$front_url);
}else{
  $total = floatval($invoice['price'])-floatval($invoice['user_credit'])+floatval($invoice['additional_cost'])+floatval($invoice['delivery_cost']);
  $retrunUrl = $payment_url."/payment_completed.php?guide=".$invoice['guide_number'];
  header('Location: '.$payment_url.'/testcard.php?total='.$total.'&retrunUrl='.$retrunUrl);
}

?>
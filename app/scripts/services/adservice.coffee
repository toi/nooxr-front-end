'use strict'

angular.module('nooxrApp')
  .factory 'adService', (Restangular) ->
    {
      createAd: (ad)->
        Restangular.all('ads').post(ad)
      getAds:(options)->
        #category_id,console_id,page,per_page,desc,order_by
        Restangular.all('ads').customGET('', options)
      getAd:(id,options)->
        Restangular.one('ad',id).get(options)
      createDeal: (ad_id,deal)->
        Restangular.all("ads/#{ad_id}/deals").post(deal)
      getAdDeals: (ad_id)->
        Restangular.one("ads/#{ad_id}/deals").get()
      getAdsByGame: (game_id)->
        Restangular.one("game/#{game_id}/ads").get()
      buy:(ad_id)->
        Restangular.all("ads/#{ad_id}/deals/buy").post()
      editAd:(obj)->
        Restangular.one("ad/#{obj.id}").customOperation('patch', null, null, null, obj)
      republishAd:(ad_id)->
        Restangular.all("republish/ad/#{ad_id}").post()
    }

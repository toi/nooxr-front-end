'use strict'

angular.module('nooxrApp')
  .factory 'paymentService', (Restangular) ->
   {
      initializePayment: (obj)->
        Restangular.all('payment/initialize').post(obj)
      createAndCompletePayment: (obj)->
        Restangular.all('payment/create-and-complete').post(obj)
      getPaymentTypes:()->
        Restangular.one('payment-options').get()
    }

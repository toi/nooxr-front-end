'use strict'

angular.module('nooxrApp')
  .factory 'validationService', ()->
    service = {}
    service.validate = (fieldsData,requiredFields,opts)->
      fieldsData = fieldsData || []
      validateInputs = requiredFields || []
      opts = opts || {}

      validation = []
      validation.ok = true

      #Empty fields Validation
      angular.forEach validateInputs, (val,$i)->
        if !fieldsData[val]
          validation[val] = true
          validation.ok = false

      #Confirm if two fields have the same data
      if opts.confirm
        if fieldsData[opts.confirm[0]] != fieldsData[opts.confirm[1]]
          validation[opts.confirm[0]] = true
          validation[opts.confirm[1]] = true
          validation.ok = false

      #Check if some field has email pattern
      if opts.checkEmailPattern
        patternEmail = /^[a-zA-Z0-9\-]{1}([\.\_\-]{0,}[a-zA-Z0-9\-]{1}){0,}@[a-zA-Z0-9]{1,}([\-]{0,}[a-zA-Z0-9]{1,}){0,}([\.]{1}[a-zA-Z0-9]{1,}){1,}$/
        if !patternEmail.test(fieldsData[opts.checkEmailPattern])
          validation[opts.checkEmailPattern] = true
          validation.ok = false

      if opts.min
        if fieldsData[opts.min[0]] < opts.min[1]
          validation[opts.min[0]] = true
          validation.msg = [opts.min[0],'min',opts.min[2]]
          validation.ok = false

      if opts.max
        if fieldsData[opts.max[0]] > opts.max[1]
          validation[opts.max[0]] = true
          validation.msg = [opts.max[0],'max',opts.max[2]]
          validation.ok = false

      return validation

    return service
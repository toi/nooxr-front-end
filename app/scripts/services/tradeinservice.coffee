'use strict'

angular.module('nooxrApp')
  .factory 'tradeinService', (Restangular) ->
    {
      getRemoteUpc: (upc) ->
        Restangular.one('tradein/remote',upc).get()
      createTrade: (trade)->
        Restangular.all('tradeins').post(trade)
    }

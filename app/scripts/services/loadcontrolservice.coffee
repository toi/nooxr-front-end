'use strict'

angular.module('nooxrApp')
  .factory 'loadcontrolService', ($rootScope,$timeout) ->
    {
      startLoad: ()->
        $rootScope.loadBarWidth = 1
        $timeout ()->
          $rootScope.loadBarWidth = Math.random() * (50 - 10) + 10
          $rootScope.spinLoad = false
        ,100

      endLoad: ()->
        $rootScope.loadBarWidth = 100
        $timeout ()->
          $rootScope.loadBarWidth = 0
        ,700

    }
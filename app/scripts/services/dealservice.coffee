'use strict'

angular.module('nooxrApp')
  .factory 'dealService', (Restangular) ->
    {
      acceptDeal: (id) ->
        Restangular.all("deal/accepted/#{id}").post()
        
      declinedDeal: (id,text)->
        Restangular.all("deal/declined/#{id}").post({text:text})
    }

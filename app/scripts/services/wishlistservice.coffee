'use strict'

angular.module('nooxrApp')
  .factory 'wishlistService', (Restangular) ->
    {
      getWishlist: () ->
        Restangular.one('user/wishlist').getList()
      addGameToWishlist: (game_id,game_name)->
        Restangular.all('user/wishlist').post({game_id:game_id,game_name:game_name})
      removeGameFromWishList: (wishlist_id)->
        Restangular.one('user/wishlist').remove({wishlist_id:wishlist_id})
      getWishlistUser:(id)->
        Restangular.one('user/wishlist',id).getList()
    }
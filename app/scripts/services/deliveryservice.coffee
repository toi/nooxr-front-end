'use strict'

angular.module('nooxrApp')
  .factory 'deliveryService', (Restangular) ->
    {
      getDeliveryServices: () ->
        Restangular.all("delivery/services").getList()
    }
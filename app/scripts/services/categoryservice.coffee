'use strict'

angular.module('nooxrApp')
  .factory 'categoryService', (Restangular) ->
    {
      getCategories: () ->
        Restangular.all("categories").getList()
    }

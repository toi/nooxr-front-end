'use strict'

angular.module('nooxrApp')
  .factory 'globalService', (Restangular,$modal) ->
    {
      getCountry: (id)->
        Restangular.one("country",id).get()
      getGameStates: ()->
        Restangular.all("game/states").getList()
      getConsoles: ()->
        Restangular.all("consoles").getList()
      showConfirmation: (data,callback)->
        $modal.open({
          template:'<div class="modal-header">'+
                      '<h6 ng-if="modalData.title">{{modalData.title}}</h6>' +
                    '</div>'+
                      '<div class="modal-body">'+
                        '<div ng-bind-html="modalData.msg"></div>'+
                        '<div class="modal-footer">'+
                        '<button class="btn btn-danger btn-embossed" ng-class="{\'disabled\':disabledBtn}" ng-click="confirm()">{{modalData.btnLabel}}</button> '+
                        '<button class="btn btn-default btn-embossed" ng-click="close()">Cancel</button> '+
                      '</div>'+
                    '</div>'
          controller: ($scope, $modalInstance)->
            $scope.modalData = data
            if !$scope.modalData.btnLabel
              $scope.modalData.btnLabel = 'Confirmar'
            $scope.confirm = ()->
              $scope.disabledBtn = true
              callback()
              $modalInstance.close()
            $scope.close = ()->
              $modalInstance.close()
        })
      showLogOutMsg: (data,callback)->
        $modal.open({
          template:'<div class="modal-header">'+
                      '<h6>Usuario desactivado</h6>' +
                    '</div>'+
                      '<div class="modal-body">'+
                        '<div ng-bind-html="modalData.msg"></div>'+
                        '<div><br><em>Si crees que esto se trata de un error contáctanos a error@nooxr.com o al teléfono +506 4031-0889</em></div>'+
                        '<div class="modal-footer">'+
                        '<button class="btn btn-info btn-embossed" ng-class="{\'disabled\':disabledBtn}" ng-if="modalData.showEmail" ng-click="activeMe()">Re-enviar Correo</button> '+
                        '<button class="btn btn-danger btn-embossed" ng-click="logOut()">LogOut</button> '+
                      '</div>'+
                    '</div>'
          controller: ($scope, $modalInstance,$rootScope,userService)->
            $scope.modalData = data
            $scope.activeMe = ()->
              $scope.disabledBtn = true
              userService.requestActivation()
            $scope.logOut = ()->
              $rootScope.logOut()
              $modalInstance.close()
          backdrop: 'static'
          keyboard: false
        })
      showAlert: (data)->
        $modal.open({
          template:'<div class="modal-header">'+
                      '<h6 ng-if="modalData.title">{{modalData.title}}</h6>' +
                    '</div>'+
                      '<div class="modal-body">'+
                        '<div ng-bind-html="modalData.msg"></div>'+
                        '<div class="modal-footer">'+
                        '<button class="btn btn-embossed btn-primary" ng-click="close()">{{modalData.btnLabel}}</button> '+
                      '</div>'+
                    '</div>'
          controller: ($scope, $modalInstance)->
            $scope.modalData = data
            if !$scope.modalData.btnLabel
              $scope.modalData.btnLabel = 'Ok'
            $scope.close = ()->
              $modalInstance.close()
        })
      sendMsg:(data)->
        $modal.open({
          template:'<div class="modal-header">'+
                      '<h6 ng-if="modalData.title">{{modalData.title}}</h6>' +
                    '</div>'+
                      '<div class="modal-body">'+
                        '<div ng-bind-html="modalData.msg"></div>'+
                        '<div class="form-field" ng-class="{\'form-group has-error\':validation.postMsg}">'+
                          '<textarea cols="30" ng-model="$parent.postMsg" rows="3" class="form-control"></textarea>'+
                        '</div>'+
                        '<div class="modal-footer">'+
                        '<button class="btn btn-danger btn-embossed" ng-click="send()">{{modalData.btnLabel}}</button> '+
                        '<button class="btn btn-default btn-embossed" ng-click="close()">Cancel</button> '+
                      '</div>'+
                    '</div>'
          controller: ($scope, $modalInstance, $location, Restangular)->
            $scope.validation = {}
            $scope.modalData = data
            if !$scope.modalData.btnLabel
              $scope.modalData.btnLabel = 'Confirmar'
            $scope.send = ()->
              if $scope.postMsg && $scope.postMsg != ''
                Restangular.all('message').post({
                    msg:$scope.postMsg
                    url:$location.$$path
                    subject:$scope.modalData.subject
                  }).then (data)->
                    $modalInstance.close()
              else 
                $scope.validation.postMsg = true
            $scope.close = ()->
              $modalInstance.close()
        }) 
    }

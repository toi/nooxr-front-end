'use strict'

angular.module('nooxrApp')
  .factory 'userService', (Restangular,$rootScope) ->
    if localStorage.token
      Restangular.setDefaultRequestParams({token: localStorage.token})
    activeUser = {}
    {
      getActiveUser: () ->
        Restangular.one('user/me').get()
      getUser: (id)->
        Restangular.one('user').get({user_id:id})
      getUserHistory: (id,options)->
        Restangular.one('user/history', id).get(options)
      getUserAds: (id,options)->
        Restangular.one('user/ads', id).get(options)
      loginUser: (email,password)->
        Restangular.all('users/login').post({email:email,password:password})
      loginUserWithKey: (a,b)->
        ab = CryptoJS.MD5("#{a}:#{b}").toString()
        Restangular.all('users/login').post({email:a,rand_key:ab})
      logOut: ()->
        Restangular.all('users/logout').post()
      setActiveUserData: (data)->
        activeUser = data
      getActiveUserData: ()->
        activeUser
      updateActiveUserData: (elem)->
        Restangular.all('user').customOperation('patch', '', '', '', elem)
      checkAvailability: (obj)->
        Restangular.one('check').get(obj)
      createUser: (user)->
        Restangular.all('users').post(user)
      setNewToken: (token)->
        localStorage.token = token
        Restangular.setDefaultRequestParams({token: localStorage.token})
      isUserLoged: (response)->
        _self = this
        _self.getActiveUser().then (data)->
          if data.error
            _self.setNewToken('')
            _self.setActiveUserData {}
            $rootScope.activeUser = ''
            response null
          else
            _self.setActiveUserData data
            $rootScope.activeUser = data
            response data
        ,(error)->
          _self.setNewToken('')
          response null
      getUserActiveAds:(options)->
        Restangular.one('user/ads/active').get(options)
      getUserfinishedAds:(options)->
        Restangular.one('user/ads/finished').get(options)
      getUserPurchases: (options)->
        Restangular.one('user/ads/purchases').get(options)
      postUserRate:(obj)->
        Restangular.all("user/rate/#{obj.id}").customOperation('patch', '', '', '', obj)
      getNotifications:(options)->
        Restangular.one('user/notifications').get(options)
      getTransactions:(options)->
        Restangular.one('user/transactions').get(options)
      getTradeins:(options)->
        Restangular.one('user/tradeins').get(options)
      notificatioskAsRead:()->
        Restangular.all('user/notifications/read').post()
      requestActivation:()->
        Restangular.all('user/activation/send').post()
      confirmActivation:(activation_id)->
        Restangular.all("user/activation/confirm/#{activation_id}").post()
      requestPasswordChange:(email)->
        Restangular.all('user/passchange/send').post({email:email})
      confirmPasswordChange:(pass,recovery_code)->
        Restangular.all("user/passchange/confirm/#{recovery_code}").post({pass:pass})
      getLastDelivery:()->
        Restangular.one('user/deliveries/last').get()
      getUserOrderById:(id)->
        Restangular.one('user/order/id',id).get()
      getUserOrderByGuide:(guide)->
        Restangular.one('user/order/guide',guide).get()
    }

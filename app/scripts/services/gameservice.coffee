'use strict'

angular.module('nooxrApp')
  .factory 'gameService', (Restangular,$http) ->
    {
      searchGames: (options)->
        Restangular.all('games').customGET('', options)
      getGame:(id,options)->
        Restangular.one('game',id).get(options)
      getCoverFromYoutube:(query)->
        $http.jsonp 'http://gdata.youtube.com/feeds/videos?alt=json-in-script&callback=JSON_CALLBACK', 
          params:{
            'start-index':1,
            'max-results': 1,
            racy: 'exclude',
            format: '5',
            orderby: 'relevance',
            vq: "#{query} trailer",
          }
      trackGameView:(game_id)->
        Restangular.one('games/view').get({game_id:game_id})

    }

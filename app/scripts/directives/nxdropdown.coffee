'use strict'

angular.module('nooxrApp')
  .directive('nxDropdown', () ->
    restrict: 'A'
    link: (scope, element, attrs) ->
      element.dropdown()
  )

'use strict'

angular.module('nooxrApp')
  .directive('nxSlider', () ->
    restrict: 'A'
    scope:
      sliderValue:'='
    link: (scope, element, attrs) ->
      $(element).ionRangeSlider
        values: [
          "Sin caja"
          "Regular"
          "Como nuevo"
          "Nuevo"
        ]
        # min: 1
        # max: 4
        type: "single"
        # step: 1
        from: 2
        hasGrid: true
        onLoad: (obj) -> # callback is called after slider load and update
          scope.sliderValue = obj.fromNumber
        # console.log(obj);
        onChange: (obj) -> # callback is called on every slider change
          scope.sliderValue = obj.fromNumber
          scope.$apply()
          return

        onFinish: (obj) -> # callback is called on slider action is finished
          scope.sliderValue = obj.fromNumber
          scope.$apply()
          return
  )

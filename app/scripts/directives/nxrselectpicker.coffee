'use strict'

angular.module('nooxrApp')
  .directive('nxrselectpicker', ($timeout)->
    restrict: 'A'
    scope:
      configselectpicker :'='
      updateCallback : '='
    link: (scope, element, attrs) ->
      $timeout ->
        $(element).select2
          dropdownCssClass: scope.configselectpicker.dropdownCssClass

      scope.updateCallback = (value)->
        $timeout ->
          $(element).select2("val", value)
  )

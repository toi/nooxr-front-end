'use strict'

angular.module('nooxrApp')
  .directive('gameVideo', ($interpolate) ->
    templateUrl: 'views/directives/game-video.html'
    restrict: 'E'
    scope:
      video : '=videoData'
    link: (scope, element, attrs) ->

      youtubeParser = (url)->
        regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        match = url.match regExp
        if match&&match[7].length==11
          b = match[7]
          return b
        else
          return "error"

      if scope.video?.indexOf('youtube') >= 0
        scope.type = 'youtube'
        videoId = youtubeParser scope.video
        scope.videoUrl = "//www.youtube.com/embed/#{videoId}"
        

  )

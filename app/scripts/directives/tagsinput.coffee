'use strict'

angular.module('nooxrApp')
  .directive('tagsinput', ->
    restrict: 'A'
    scope: {
      data : '='
    }
    link: (scope, element, attrs) ->
      
      $(element).tagsinput
  )

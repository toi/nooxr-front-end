'use strict'

angular.module('nooxrApp')
  .directive('notice', ->
    templateUrl: 'views/directives/notice.html'
    restrict: 'E',
    scope : {
      type: '@noticeType'
      text: '@noticeText'
    }
    link: (scope, element, attrs) ->
      
  )

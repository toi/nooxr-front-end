'use strict'

angular.module('nooxrApp')
  .directive('gameBoxSmall', () ->
    templateUrl: 'views/directives/game-box-small.html'
    restrict: 'E'
    scope: 
      game: '=gameData'
    link: (scope, element, attrs) ->
  )

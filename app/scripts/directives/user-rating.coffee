'use strict'

angular.module('nooxrApp')
  .directive('userRating', () ->
    templateUrl: 'views/directives/user-rating.html'
    scope: {
      state : '='
      activeRating: '@'
      onChange: '='
      extraData: '='
    }
    restrict: 'E'
    link: (scope, element, attrs) ->
      scope.changeState = (state)->
        scope.state = state
        scope.onChange(scope.extraData,state)
  )

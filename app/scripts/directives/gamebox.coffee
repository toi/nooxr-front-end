'use strict'

angular.module('nooxrApp')
  .directive('gameBox', () ->
    templateUrl: 'views/directives/game-box.html'
    restrict: 'E'
    scope:
      game: '=gameData'
    link: (scope, element, attrs) ->

      htmlToPlaintext =(text)->
        return String(text).replace(/<[^>]+>/gm, '')

      scope.game.description = htmlToPlaintext(scope.game.description)
       
  )

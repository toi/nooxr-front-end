'use strict'

angular.module('nooxrApp')
  .directive('currencyInput', () ->
    restrict: 'A'
    scope: {
      number: '=ngModel'
    }
    controller: ($scope, $element, $attrs) ->
      $scope.$watch 'number', (a,b)->
        $($element).val("#{$scope.number}")
        addFormat()

      addFormat = (applytoScope)->
        _val = $($element).val()
        if "#{_val}" != ''
          if _val[0] != "₡"
            _val = "$#{_val}"
          
          decimal_num = _val.split('.')
          if decimal_num[1]
            _val = decimal_num[0]
            decimal_num = decimal_num[1]
            decimal_num = parseInt(decimal_num)
          else
            decimal_num = false

          _haspoint = _val.indexOf(".")

          arr = _val.match /.{1,1}/g
          _arr = []
          _arrNoComma = []
          module = 0
          arr.reverse()
          angular.forEach arr, (val,i)->
            if parseInt(val) >= 0
              module = module+1
              _arr.push val
              _arrNoComma.push val
              if module%3 == 0
                if i+2 != arr.length
                  _arr.push ','
          if _arr.length > 0
            arr = _arr.reverse()
            newValue = arr.join('')
            _exitNum = parseFloat(_arrNoComma.reverse().join(''))
          else
            newValue = ""
            _exitNum = ""

          if decimal_num
            $($element).val("₡#{newValue}.#{decimal_num}")
          else if _haspoint > 0
            $($element).val("₡#{newValue}.")
          else
            $($element).val("₡#{newValue}")

          if applytoScope == true
            $scope.$apply ()->
              if decimal_num
                $scope.number = parseFloat(_exitNum + '.' + decimal_num)
                if decimal_num > 99
                  $scope.number = $scope.number.toFixed(2)
              else
                $scope.number = _exitNum
          if decimal_num
              $scope.number = parseFloat(_exitNum + '.' + decimal_num)
              if decimal_num > 99
                $scope.number = $scope.number.toFixed(2)
            else
              $scope.number = _exitNum
        else
          $scope.number = ""
          if applytoScope == true
            $scope.$apply ()->
              $scope.number = ""

      $($element).on 'keyup', (e)->
        addFormat(true)

  )

'use strict'

angular.module('nooxrApp')
  .directive('accountTabs', () ->
    templateUrl: 'views/directives/account-tabs.html'
    restrict: 'E'
    scope:
      activeTab : '@'
    link: (scope, element, attrs) ->
      scope.primaryTab = if scope.activeTab.indexOf('sales') >= 0 then 'sales' else false
  )

'use strict'

angular.module('nooxrApp')
  .directive('gameBoxSmallProfile', () ->
    templateUrl: 'views/directives/game-box-small-profile.html'
    restrict: 'E'
    scope:
      game: '=gameData'
    link: (scope, element, attrs) ->
  )

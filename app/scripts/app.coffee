'use strict'

angular.module('nooxrApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'restangular',
  'facebook',
  'ui.select2',
  'ui.keypress',
  'html5Uploader',
  'ui.bootstrap'
])
  .config ($routeProvider,RestangularProvider,FacebookProvider,$sceProvider) ->
    $sceProvider.enabled false 
    RestangularProvider.setBaseUrl('http://localhost:3000/v1/')
    # RestangularProvider.setBaseUrl('http://api.cr.nooxr.com/v1/')
    
    RestangularProvider.setDefaultHeaders {Authorization:"Basic bm9veHI6MTIzMTIzdG9pJA=="}

    FacebookProvider.init '669472639762201'

    requiereAuth = ($q,globalService,Facebook,userService,$rootScope,$location,isLogin,afterLogin)->
      redirectTo = if isLogin then '/' else '/login'
      defer = $q.defer()
      if localStorage.token
        userService.getActiveUser().then (d)->
          if !d.error
            if d.active == false
              _msg = if !d.deactivated_reason then 'Tenes que activar tu usuario para utilizar este sitio. Para activarlo haz click en el link que te enviamos a tu correo electronico.' else "Desactivamos tu usuario por la siguiente razon: <br>#{d.deactivated_reason}" 
              _sEmail = if !d.deactivated_reason then true else false
              globalService.showLogOutMsg({
                msg:_msg
                showEmail:_sEmail
              })
            userService.setActiveUserData d
            $rootScope.activeUser = d
            if isLogin
              $location.path(redirectTo)
            else
              defer.resolve(false)
          else
            $rootScope.activeUser = null
            Facebook.getLoginStatus (response)->
              if response.status == 'connected'
                Facebook.logout()
              if isLogin
                defer.resolve(false)
              else
                localStorage.afterLogin = afterLogin
                $location.path(redirectTo)
      else
        $rootScope.activeUser = null
        Facebook.getLoginStatus (response)->
          if response.status == 'connected'
            Facebook.logout()
          if isLogin
            defer.resolve(false)
          else
            localStorage.afterLogin = afterLogin
            $location.path(redirectTo)
      defer.promise

    $routeProvider
      .when '/games',
        templateUrl: 'views/main.html'
        controller: 'MainCtrl'
        resolve:
          categories: (categoryService)->
            categoryService.getCategories()
          ads: (adService, $route)->
            adService.getAds {
              page:$route.current.params.page || 1
              page_limit:$route.current.params.page_limit || 15
              desc:$route.current.params.desc || true
              console_id:$route.current.params.console_id if $route.current.params.console_id
              order_by:$route.current.params.order_by if $route.current.params.order_by
            }
      .when '/login',
        templateUrl: 'views/login.html'
        controller: 'LoginCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,true)
      .when '/register',
        templateUrl: 'views/register.html'
        controller: 'RegisterCtrl'
        resolve:
          country: (globalService)->
            globalService.getCountry(1)
          auth:($q,Facebook,userService,$rootScope,$location,globalService)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,true)
      .when '/account',
        templateUrl: 'views/account.html'
        controller: 'AccountCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/account')
          logedUser:($q,userService)->
            defer = $q.defer()
            userService.isUserLoged (data)->
              defer.resolve(data)
            defer.promise
          country: (globalService)->
            globalService.getCountry(1)
      .when '/add-ad',
        templateUrl: 'views/add-ad.html'
        controller: 'AddAdCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/add-ad')
          logedUser:($q,userService)->
            defer = $q.defer()
            userService.isUserLoged (data)->
              defer.resolve(data)
            defer.promise
          gameStates:(globalService)->
            globalService.getGameStates()
          consoles:(globalService)->
            globalService.getConsoles()
      .when '/product/:product_id',
        templateUrl: 'views/product.html'
        controller: 'ProductCtrl'
        resolve:
          product:(gameService,$route)->
            gameService.getGame $route.current.params.product_id, {
              result_size:"large"
              with:"other_games"
            }
          ads:(adService,$route)->
            adService.getAdsByGame $route.current.params.product_id, {
              page_limit: 100
            }
      .when '/games/category/:category_id',
        templateUrl: 'views/main.html'
        controller: 'MainCtrl'
        resolve:
          categories: (categoryService)->
              categoryService.getCategories()
          ads: (adService, $route)->
            adService.getAds {
              category_id:$route.current.params.category_id
              page:$route.current.params.page || 1
              page_limit:$route.current.params.page_limit || 16
              desc:$route.current.params.desc || true
              console_id:$route.current.params.console_id if $route.current.params.console_id
              order_by:$route.current.params.order_by if $route.current.params.order_by
            }
      .when '/games/search/:search',
        templateUrl: 'views/main.html'
        controller: 'MainCtrl'
        resolve:
          categories: (categoryService)->
            categoryService.getCategories()
          ads:(gameService,$route)->
            gameService.searchGames {
              search:$route.current.params.search
              page:$route.current.params.page || 1
              page_limit:$route.current.params.page_limit || 16
              desc:$route.current.params.desc || true
              console_id:$route.current.params.console_id if $route.current.params.console_id
              order_by:$route.current.params.order_by if $route.current.params.order_by
              result_size:"medium"
            }
      .when '/profile/:profile_id',
        templateUrl: 'views/profile.html'
        controller: 'ProfileCtrl'
        resolve:
          profile:(userService,$route)->
            userService.getUser($route.current.params.profile_id)
          userAds:(userService,$route)->
            userService.getUserAds($route.current.params.profile_id,{
              page_limit:$route.current.params.s_page_limit || 12
              page:$route.current.params.s_page || 1
              desc:true
            })
          userHistory:(userService,$route)->
            userService.getUserHistory($route.current.params.profile_id,{
              page_limit:$route.current.params.h_page_limit || 12
              page:$route.current.params.h_page || 1
              desc:true
            })
          wishlist:(wishlistService,$route)->
            wishlistService.getWishlistUser($route.current.params.profile_id)
      .when '/ad/:ad_id',
        templateUrl: 'views/offer.html'
        controller: 'OfferCtrl'
        resolve:
          logedUser:($q,userService,globalService)->
            defer = $q.defer()
            userService.isUserLoged (d)->              
              if d?.active == false
                _msg = if !d.deactivated_reason then 'Tienes que activar tu usuario haciendo click en el link que te enviamos a tu correo electronico.' else "Desactivamos tu usuario por la siguiente razon: <br>#{d.deactivated_reason}" 
                _sEmail = if !d.deactivated_reason then true else false
                globalService.showLogOutMsg({
                  msg:_msg
                  showEmail:_sEmail
                })
              defer.resolve(d)
            defer.promise
          ad: (adService,$route)->
            adService.getAd $route.current.params.ad_id, {
              result_size:"large"
              with:"other_ads_console"
            }
          adDeals:(adService,$route)->
            adService.getAdDeals $route.current.params.ad_id
      .when '/wishlist',
        templateUrl: 'views/wishlist.html'
        controller: 'WishlistCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/wishlist')
          wishlist:(wishlistService)->
            wishlistService.getWishlist()
      .when '/sales/active',
        templateUrl: 'views/sales/active.html'
        controller: 'SalesActiveCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/sales/active')
          data:(userService,$route)->
            userService.getUserActiveAds({
                page:$route.current.params.page || 1
                page_limit:$route.current.params.page_limit || 10
                desc:true
              })
          aditionalAd:($route,userService,adService,$q)->
            defer = $q.defer()
            if $route.current.params.open_ad
              adService.getAd($route.current.params.open_ad).then (data)->
                userService.getActiveUser().then (_user)->
                  if data.user.user_id == _user.user_id
                    defer.resolve(data)
                  else
                    defer.resolve(null)
            else
              defer.resolve(null)
            defer.promise
          gameStates:(globalService)->
            globalService.getGameStates()
          consoles:(globalService)->
            globalService.getConsoles()
      .when '/sales/finished',
        templateUrl: 'views/sales/finished.html'
        controller: 'SalesFinishedCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/sales/finished')
          data:(userService,$route)->
            userService.getUserfinishedAds({
                page:$route.current.params.page || 1
                page_limit:$route.current.params.page_limit || 10
                desc:true
              })
      .when '/purchase',
        templateUrl: 'views/purchase.html'
        controller: 'PurchaseCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/purchase')
          data:(userService,$route)->
            userService.getUserPurchases({
                page:$route.current.params.page || 1
                page_limit:$route.current.params.page_limit || 10
                desc:true
              })
      .when '/terms',
        templateUrl: 'views/terms.html'
        controller: 'TermsCtrl'
      .when '/how-it-works',
        templateUrl: 'views/howitworks.html'
        controller: 'HowitworksCtrl'
      .when '/tips',
        templateUrl: 'views/tips.html'
        controller: 'TipsCtrl'
      .when '/contact-us',
        templateUrl: 'views/contact-us.html'
        controller: 'ContactUsCtrl'
        resolve:
          logedUser:($q,userService,globalService)->
              defer = $q.defer()
              userService.isUserLoged (d)->              
                if d?.active == false
                  _msg = if !d.deactivated_reason then 'Tienes que activar tu usuario haciendo click en el link que te enviamos a tu correo electronico.' else "Desactivamos tu usuario por la siguiente razon: <br>#{d.deactivated_reason}" 
                  _sEmail = if !d.deactivated_reason then true else false
                  globalService.showLogOutMsg({
                    msg:_msg
                    showEmail:_sEmail
                  })
                defer.resolve(d)
              defer.promise
      .when '/confirm/:confirm_code',
        templateUrl: 'views/confirm.html'
        controller: 'ConfirmCtrl'
        resolve:
          confirmation:(userService,$route)->
            userService.confirmActivation $route.current.params.confirm_code
      .when '/passchange/:recovery_code',
        templateUrl: 'views/passchange.html'
        controller: 'PasschangeCtrl'
      .when '/limitations',
        templateUrl: 'views/limitations.html'
        controller: 'LimitationsCtrl'
      .when '/notifications',
        templateUrl: 'views/notifications.html'
        controller: 'NotificationsCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/notifications')
          notifications:(userService,$route)->
            userService.getNotifications({
              page:$route.current.params.page || 1
              page_limit:$route.current.params.page_limit || 15
              desc:true
            })
      .when '/tradein',
        templateUrl: 'views/tradein-soon.html'
        controller: 'TradeinCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/tradein')
          logedUser:($q,userService,globalService)->
            defer = $q.defer()
            userService.isUserLoged (d)->              
              if d?.active == false
                _msg = if !d.deactivated_reason then 'Tienes que activar tu usuario haciendo click en el link que te enviamos a tu correo electronico.' else "Desactivamos tu usuario por la siguiente razon: <br>#{d.deactivated_reason}" 
                _sEmail = if !d.deactivated_reason then true else false
                globalService.showLogOutMsg({
                  msg:_msg
                  showEmail:_sEmail
                })
              defer.resolve(d)
            defer.promise
      .when '/payment/:ad_id',
        templateUrl: 'views/payment.html'
        controller: 'PaymentCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService,$route)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/payment/'+$route.current.params.ad_id)
          logedUser:($q,userService,globalService)->
            defer = $q.defer()
            userService.isUserLoged (d)->              
              if d?.active == false
                _msg = if !d.deactivated_reason then 'Tienes que activar tu usuario haciendo click en el link que te enviamos a tu correo electronico.' else "Desactivamos tu usuario por la siguiente razon: <br>#{d.deactivated_reason}" 
                _sEmail = if !d.deactivated_reason then true else false
                globalService.showLogOutMsg({
                  msg:_msg
                  showEmail:_sEmail
                })
              defer.resolve(d)
            defer.promise
          ad: (adService,$route)->
            adService.getAd $route.current.params.ad_id, {
              result_size:"large"
              with:"other_ads_console"
            }
          deliveryServices:(deliveryService)->
            deliveryService.getDeliveryServices()
          lastDelivery:(userService)->
            userService.getLastDelivery()
          paymentOptions:(paymentService)->
            paymentService.getPaymentTypes()
      .when '/transactions',
        templateUrl: 'views/transactions.html'
        controller: 'TransactionsCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/transactions')
          transactions:(userService,$route)->
            userService.getTransactions({
              page:$route.current.params.page || 1
              page_limit:$route.current.params.page_limit || 15
              desc:true
            })
      .when '/tradeins',
        templateUrl: 'views/tradeins.html'
        controller: 'TradeinsCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/tradeins')
          tradeins:(userService,$route)->
            userService.getTradeins({
              page:$route.current.params.page || 1
              page_limit:$route.current.params.page_limit || 15
              desc:true
            })
      .when '/payment-completed/:guide_id',
        templateUrl: 'views/payment-completed.html'
        controller: 'PaymentCompletedCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService,$route)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/payment-completed/'+$route.current.params.guide_id)
          logedUser:($q,userService,globalService)->
            defer = $q.defer()
            userService.isUserLoged (d)->              
              if d?.active == false
                _msg = if !d.deactivated_reason then 'Tienes que activar tu usuario haciendo click en el link que te enviamos a tu correo electronico.' else "Desactivamos tu usuario por la siguiente razon: <br>#{d.deactivated_reason}" 
                _sEmail = if !d.deactivated_reason then true else false
                globalService.showLogOutMsg({
                  msg:_msg
                  showEmail:_sEmail
                })
              defer.resolve(d)
            defer.promise
          order:(userService,$route)->
            userService.getUserOrderByGuide($route.current.params.guide_id)
      .when '/order-detail/:order_id',
        templateUrl: 'views/order-detail.html'
        controller: 'OrderDetailCtrl'
        resolve:
          auth:($q,Facebook,userService,$rootScope,$location,globalService,$route)->
            requiereAuth($q,globalService,Facebook,userService,$rootScope,$location,false,'/order-detail/'+$route.current.params.order_id)
          logedUser:($q,userService,globalService)->
            defer = $q.defer()
            userService.isUserLoged (d)->              
              if d?.active == false
                _msg = if !d.deactivated_reason then 'Tienes que activar tu usuario haciendo click en el link que te enviamos a tu correo electronico.' else "Desactivamos tu usuario por la siguiente razon: <br>#{d.deactivated_reason}" 
                _sEmail = if !d.deactivated_reason then true else false
                globalService.showLogOutMsg({
                  msg:_msg
                  showEmail:_sEmail
                })
              defer.resolve(d)
            defer.promise
          order:(userService,$route)->
            userService.getUserOrderById($route.current.params.order_id)
      .otherwise
        redirectTo: '/games'
  .run ($rootScope, userService, $location, Facebook, $timeout, globalService, loadcontrolService, $route, categoryService)->

    $rootScope.userShowConfirmAlert = false
    $timeout ->
      $rootScope.userShowConfirmAlert = true
    ,200

    $rootScope.resendActivation = ()->
      userService.requestActivation().then ()->
        globalService.showAlert {title:'Confirmación reenviada',msg:'El mensaje de confirmación a sido reenviado tu correo, por favor sigue las instrucciones en el mismo para confirmar tu cuenta'}

    globalService.getConsoles().then (data)->
      $rootScope.menuConsoles = data

    categoryService.getCategories().then (data)->
      $rootScope.menuCategories = data
    
    $rootScope.logOut = ()->
      userService.logOut().then (data)->
        userService.setNewToken('')
        Facebook.getLoginStatus (response)->
          if response.status == 'connected'
            Facebook.logout()
        $location.path('/')
        userService.setActiveUserData {}
        $rootScope.activeUser = ''

    $rootScope.logIn = (goTo)->
      redirectTo = if goTo then goTo else ''
      localStorage.afterLogin = redirectTo
      $location.path('/login')

    userService.isUserLoged ()->
    localStorage.afterLogin = ''

    $rootScope.search = ()->
      $location.path("/games/search/#{$rootScope.searchInput}")
      $timeout ()->
        $rootScope.searchInput = ''
      ,50

    $rootScope.$on '$locationChangeStart', ()->
      loadcontrolService.startLoad()
    
    $rootScope.$on '$routeChangeSuccess', ()->
      window.scrollTo(0,0);
      $rootScope.actualPath = $location.$$path

    $rootScope.$on '$locationChangeSuccess', ()->
      ga('send', 'pageview', $location.path())
      $rootScope.urlParams = $route.current.params

      

'use strict'

angular.module('nooxrApp')
  .controller 'PurchaseCtrl', ($scope,loadcontrolService,data,$modal,$route,$location,globalService) ->
    loadcontrolService.endLoad()
    # User list of games
    $scope.ads = data.results

    $scope.pActualPage = $route.current.params.page || 1
    $scope.pNumPages = data.pages
    $scope.pTotalItems = data.result_count
    $scope.pItemsPerPage = $route.current.params.page_limit || 10
    $scope.pPageChange = (page)->
      $location.search('page',page).path("#{$location.$$path}")

    $scope.tempRates = []

    $scope.rateUser = (rate,score)->
      $scope.validation = {}
      $scope.rateComment = ''
      $scope.newRateData = 
        id:rate.id
        score:score
      rateUser = $modal.open({
        templateUrl:'rateUserModal'
        scope:$scope
        controller: ($scope, $modalInstance, userService)->
          $scope.cancel = ()->
            $modalInstance.close()
          $scope.rateUser = ()->
            if $scope.rateComment
              obj = {
                id:$scope.newRateData.id
                score:$scope.newRateData.score
                comment:$scope.rateComment
              }
              userService.postUserRate(obj).then (data)->
                angular.forEach $scope.ads, (val,$i)->
                  if val.user_rate.id == $scope.newRateData.id
                    $scope.ads[$i].user_rate.score = $scope.newRateData.score
                    $modalInstance.close()
            else
              $scope.validation.rateComment = true
      })

    $scope.showSellerData = (ad)->
      globalService.showAlert {title:'Datos del vendedor',msg:"Puedes contactar al vendedor al:<br><br>Teléfono: #{ad.user_rate.user_extra.phone}<br>Email: #{ad.user_rate.user_extra.email}"}
'use strict'

angular.module('nooxrApp')
  .controller 'AddAdCtrl', ($scope, logedUser, Restangular,gameStates, consoles, $rootScope, userService,adService,$location,loadcontrolService,globalService) ->
    loadcontrolService.endLoad()

    if !logedUser.confirmed
      globalService.showAlert {title:'Usuario no confirmado',msg:'Tienes que confirmar tu usuario para publicar anuncios'}
      return $location.path('/') 


    $scope.gameStates = gameStates
    $scope.consoles = consoles

    $scope.user = angular.copy($rootScope.activeUser)

    formatSelection = (result)->
      "<div class='big-search-result'><img src='#{result.thumbnail}'/><span>#{result.title}</span></div>"

    $scope.selectGame = {
      maximumSelectionSize : 1
      minimumInputLength: 3
      formatResult: formatSelection
      formatSelection: formatSelection
      initSelection: (element, callback)-> 
      ajax:
        url: "#{Restangular.configuration.baseUrl}/games"
        data: (term, page) ->
          return {
            search: term
            page: page
          }
        results: (data, page) ->
          return {
            results: data.results
            more: false
          }
    }

    formatResultGames = (result)->
      "#{result.title}"

    formatSelectionGames = (result)->
      result

    $scope.selectGames = {
      maximumSelectionSize : 1
      minimumInputLength: 3
      formatResult: formatResultGames
      formatSelection: formatResultGames
      initSelection: (element, callback)-> 
      ajax:
        url: "#{Restangular.configuration.baseUrl}/games"
        data: (term, page) ->
          return {
            search: term
            page: page
          }
        results: (data, page) ->
          return {
            results: data.results
            more: false
          }
    }

    $scope.acceptedGames = []
    tagCounterAcceptedGames = 0

    $scope.showAddGame = 0

    $scope.$watch 'showAddGame', (a,b)->
      $scope.addAcceptedGameDB = ''

    $scope.$watch 'addAcceptedGameDB', (a,b)->
      if typeof a == 'object' && a?.title != '' && a
        _exist = _.where $scope.acceptedGames, { id: a?.id }
        if _exist.length == 0
          tagCounterAcceptedGames = tagCounterAcceptedGames+1
          $scope.acceptedGames.push
            count: tagCounterAcceptedGames
            id:a?.id
            title:a?.title
        $scope.showAddGame = 0


    $scope.addGameNoDB = ()->
      tagCounterAcceptedGames = tagCounterAcceptedGames+1
      $scope.acceptedGames.push {title:$scope.addAcceptedGame, count:tagCounterAcceptedGames}
      $scope.addAcceptedGame = ''
      $scope.showAddGame = 0

    $scope.removeTagGame = (item)->
      $scope.acceptedGames.splice $scope.acceptedGames.indexOf(item), 1

    getValidation = ()->
      validation = {}
      validation.ok = true

      if !$scope.ad.id && !$scope.nolist
        validation.ok = false
        validation.gameId = true

      if !$scope.ad.noid && $scope.nolist
        validation.ok = false
        validation.noid = true

      if !$scope.ad.price
        validation.ok = false
        validation.price = true

      if !$scope.ad.console_id 
        validation.ok = false
        validation.console_id = true

      if !$scope.ad.gameState_id 
        validation.ok = false
        validation.gameState_id  = true

      if !$scope.user.phone
        validation.ok = false
        validation.phone = true

      if !$scope.user.city
        validation.ok = false
        validation.city = true

      return validation

    $scope.publishIt = ()->
      $scope.validation = getValidation()
      if $scope.validation.ok
        $scope.disableSendBtn = true
        userService.updateActiveUserData({phone:$scope.user.phone, city:$scope.user.city}).then (user_data)->
          $rootScope.activeUser = user_data
          $scope.ad.acceptedGames_ids = []
          $scope.ad.index_games = "$"
          $scope.ad.acceptedGames_text = "$"

          angular.forEach $scope.acceptedGames, (x,y)->
            if x.id
              $scope.ad.acceptedGames_ids.push(x.id)
            else
              $scope.ad.index_games = $scope.ad.index_games + " - #{x.title}"
            $scope.ad.acceptedGames_text = $scope.ad.acceptedGames_text + " - #{x.title}"

          $scope.ad.index_games = $scope.ad.index_games.replace('$ -','').replace('$','')
          $scope.ad.acceptedGames_text = $scope.ad.acceptedGames_text.replace('$ -','').replace('$','')
          $scope.ad.id = $scope.ad.id?.id

          adService.createAd($scope.ad).then (data)->
            if !data.error
              if data.game.id > 0
                swal({   title: "Listo!",   text: "Tu anuncio fue publicado",   type: "success",   confirmButtonText: "OK" });
              else
                swal({   title: "<small>Indexado pendiente!</small>",   text: "Estamos indexando tu juego, próximamente aparacera en los resultados de búsqueda.", type: "success", html:true, confirmButtonText: "OK" });
              $location.path '/'
            else
              swal({   title: "Error",   text: "Tu anuncio no puede ser publicado en este momento, intenta más tarde.",   type: "error",   confirmButtonText: "OK" });
              $location.path '/'
      else
        swal({   title: "<small>Campos obligatorios</small>", text: "Por favor revisa los campos obligatorios", type: "warning", html: true, confirmButtonText: "OK" });



          
          


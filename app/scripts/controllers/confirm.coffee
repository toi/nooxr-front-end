'use strict'

angular.module('nooxrApp')
  .controller 'ConfirmCtrl', ($scope,loadcontrolService,confirmation) ->
    if "#{confirmation.error}" == 'true'
      $scope.error = true
    else
      $scope.error = false
    loadcontrolService.endLoad()

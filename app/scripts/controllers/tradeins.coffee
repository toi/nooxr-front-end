'use strict'

angular.module('nooxrApp')
  .controller 'TradeinsCtrl', ($scope,loadcontrolService,tradeins,$route,$location,globalService,$filter) ->
    loadcontrolService.endLoad()

    $scope.pActualPage = $route.current.params.page || 1
    $scope.pNumPages = tradeins.pages
    $scope.pTotalItems = tradeins.result_count
    $scope.pItemsPerPage = $route.current.params.page_limit || 15
    $scope.pPageChange = (page)->
      $location.search('page',page).path("#{$location.$$path}")

    $scope.tradeins = tradeins.results

    $scope.showDetail = (t)->
      _send = $filter('timeAgo')(t.created_at)
      _price = $filter('number')(t.price,2)
      _pass = if t.with_pass then  'Sí' else 'No'
      globalService.showAlert {title:"#{t.name}",msg:"<strong>Precio: </strong>₡ #{_price} <br> <strong>Estado del juego: </strong> #{t.state} <br> <strong>Codigo de entrega: </strong> #{t.delivery_code} <br> <strong>Consola: </strong> #{t.console} <br> Tiene \"PassCode\" para jugar en internet : #{_pass} <br> <strong>Comentarios: </strong> <br> #{t.comments} <br><br><br> <strong>Enviado: </strong> #{_send}"}
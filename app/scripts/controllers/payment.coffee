'use strict'

angular.module('nooxrApp')
  .controller 'PaymentCtrl', ($scope,$location,ad,adService,loadcontrolService,logedUser,globalService,deliveryServices,paymentService,lastDelivery,$timeout,paymentOptions) ->
    loadcontrolService.endLoad()
    if ad.error == true || ad.ad.active == false || ad.ad.user.user_id == logedUser.user_id || !ad.ad.user.allow_payments || !logedUser.confirmed
      return $location.path('/') 

    if ad.ad.finished == true
      return $location.path('ad/'+ad.ad.id) 

    $scope.paymentOptions = paymentOptions
    $scope.paymentMethod = $scope.paymentOptions[0]

    _paymentSend = false
    $scope.logedUser = logedUser
    $scope.ad = ad.ad
    $scope.deliveryServices = deliveryServices
    $scope.credits = if $scope.logedUser.credits >= $scope.ad.price then $scope.ad.price else $scope.logedUser.credits
    $scope.preSubtotal =  $scope.ad.price-$scope.credits

    $scope.serviceCost = if $scope.preSubtotal > 1000 then 0 else 1000
    $scope.activeDelivery = $scope.deliveryServices[0]

    getLastDeliveryById = ()->
      if !lastDelivery.error
        findDelivery = _.where  $scope.deliveryServices, {id:parseInt(lastDelivery.delivery.delivery_service_id)}
        if findDelivery.length > 0
          $scope.activeDelivery = findDelivery[0]
        if $scope.activeDelivery.cost == 0
          $scope.lastDelivery = null
          $scope.address = $scope.logedUser.address
          $scope.phone = $scope.logedUser.phone
        else
          $scope.lastDelivery = lastDelivery.delivery
          $scope.address = $scope.lastDelivery.address
          $scope.phone = $scope.lastDelivery.phone
          $scope.lastOrder = lastDelivery.order_id
      else
        $scope.lastDelivery = null
        $scope.address = $scope.logedUser.address
        $scope.phone = $scope.logedUser.phone

    getLastDeliveryById()

    $scope.$watch 'activeDelivery', (a,b)->
      $scope.total = if $scope.lastDelivery then $scope.preSubtotal+$scope.serviceCost else $scope.preSubtotal+$scope.serviceCost+parseFloat($scope.activeDelivery.cost)

    $scope.$watch 'paymentMethod', (a,b)->
      if a.allow_deliveries == true
        #set contra entrega as default
        getLastDeliveryById()
        $scope.forceDelivery = false
      else
        $scope.forceDelivery = true
        $scope.address = "El pedido será entregado en la tienda"
        $scope.phone = $scope.logedUser.phone
        $scope.lastDelivery = null
        $scope.activeDelivery = $scope.deliveryServices[0]
        $timeout ->
          $scope.updateSelect2($scope.activeDelivery.id)


    $scope.validation = {}

    $scope.total = if $scope.lastDelivery then $scope.preSubtotal+$scope.serviceCost else $scope.preSubtotal+$scope.serviceCost+$scope.activeDelivery.cost

    $scope.removeLastDelivery = ()->
      $scope.lastDelivery = null
      $scope.address = $scope.logedUser.address
      $scope.phone = $scope.logedUser.phone
      $scope.activeDelivery = $scope.deliveryServices[0]
      $scope.updateSelect2($scope.activeDelivery.id)

    $scope.paymentloading = false

    $scope.initializePayment = ()->
      if $scope.paymentMethod.id == 1
        if $scope.address && $scope.phone
          $scope.paymentloading = true
          _payment =
            ad_id: $scope.ad.id
            deliveryService_id: $scope.activeDelivery.id
            total: $scope.total
            address: $scope.address
            phone: $scope.phone
          if $scope.lastDelivery?.id
            _payment.delivery_id = $scope.lastDelivery.id
          if _paymentSend == false
            _paymentSend = true
            paymentService.initializePayment(_payment).then (data)->
              if data.error
                location.reload()
              else
                $scope.paymentloading = false
                window.location = "http://localhost:8888/payment.php?guide=#{data.guide_number}"
        else
          $scope.validation['address'] = if $scope.address then false else true
          $scope.validation['phone'] = if $scope.phone then false else true
      else
        if $scope.phone
          $scope.paymentloading = true
          _payment =
            ad_id: $scope.ad.id
            deliveryService_id: $scope.activeDelivery.id
            total: $scope.total
            address: "El pedido será entregado en la tienda"
            phone: $scope.phone
          if $scope.lastDelivery?.id
            _payment.delivery_id = $scope.lastDelivery.id
          if _paymentSend == false
            _paymentSend = true
            paymentService.createAndCompletePayment(_payment).then (data)->
              if !data.error
                console.log data,'data'
                $location.path('payment-completed/'+data.guide_number)
              else
                _paymentSend = false
                $scope.paymentloading = false
                globalService.showAlert {title:'Error al Comprar',msg:'La compra no puede ser procesada. Error Code: '+data.text}

        else
          $scope.validation['phone'] = if $scope.phone then false else true

    
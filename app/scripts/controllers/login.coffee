'use strict'

angular.module('nooxrApp')
  .controller 'LoginCtrl', ($scope,Facebook,userService,$location,auth,loadcontrolService,$modal) ->
    loadcontrolService.endLoad()
    $scope.facebookLogin = ()->
      Facebook.login (response)->
        if response.status == 'connected'
          $scope.me()
      ,{scope: 'email'}

    $scope.me = ()->
      Facebook.api '/me', (response)->
        userService.loginUserWithKey(response.email,response.id).then (data)->
          if !data.error
            userService.setNewToken(data.token)
            goTo = if localStorage.afterLogin then localStorage.afterLogin else '/notifications'
            localStorage.afterLogin = ''
            userService.isUserLoged ()->
              $location.path(goTo)
          else
            $location.path('/register')

    getValidation = ()->
      validation = {}
      validation.ok = true

      if !$scope.email
        validation.ok = false
        validation.email = true

      if !$scope.password
        validation.ok = false
        validation.password = true

      return validation

    $scope.normalLogin = ()->
      $scope.validation = getValidation()
      if $scope.validation.ok
        userService.loginUser($scope.email,$scope.password).then (data)->
          if !data.error
            userService.setNewToken(data.token)
            goTo = if localStorage.afterLogin then localStorage.afterLogin else '/notifications'
            localStorage.afterLogin = ''
            userService.isUserLoged ()->
              $location.path(goTo)
          else
            $scope.validation.email = true
            $scope.validation.password = true
      else
        swal({   title: "Campos obligatorios",   text: "Todos los campos son obligatorios",   type: "warning",   confirmButtonText: "OK" });

    $scope.passRecovery = (rate,score)->
      swal({
        title: "<small>Recuperar contraseña</small>",
        text: "Escribe el correo con el que te registraste:",
        type: "input",
        html: true,
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Correo electrónico" }, 
        (inputValue)->
          if inputValue == false
            return false
          else
            if inputValue == ""
              swal.showInputError("Escribe tu correo")
              return false
            else
              userService.requestPasswordChange(inputValue).then ()->
                swal("Listo!", "Te hemos enviado un mensaje al correo: " + inputValue, "success")
            
      );               

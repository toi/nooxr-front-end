'use strict'

angular.module('nooxrApp')
  .controller 'ProfileCtrl', ($scope,loadcontrolService,profile,userAds,userHistory,$location,wishlist,$route) ->

    loadcontrolService.endLoad()
    if profile.error == true
      return $location.path('/') 


    $scope.psActualPage = $route.current.params.s_page || 1
    $scope.psNumPages = userAds.pages
    $scope.psTotalItems = userAds.result_count
    $scope.psItemsPerPage = $route.current.params.s_page_limit || 12
    $scope.psPageChange = (page)->
      _search = 
        's_page':page
        'init_tab':$scope.tab
      if $route.current.params.s_page_limit
        _search.s_page_limit = $scope.psItemsPerPage
      $location.search(_search).path("#{$location.$$path}")

    $scope.phActualPage = $route.current.params.h_page || 1
    $scope.phNumPages = userHistory.pages
    $scope.phTotalItems = userHistory.result_count
    $scope.phItemsPerPage = $route.current.params.h_page_limit || 12
    $scope.phPageChange = (page)->
      _search = 
        'h_page':page
        'init_tab':$scope.tab
      if $route.current.params.h_page_limit
        _search.h_page_limit = $scope.phItemsPerPage
      $location.search(_search).path("#{$location.$$path}")

    $scope.profile = profile
    
    # Inicial tab
    $scope.tab = $route.current.params.init_tab || 1

    # User list of games
    $scope.ads = userAds.results

    # Users reputation
    $scope.history_rates = userHistory.results

    $scope.wishlists = wishlist
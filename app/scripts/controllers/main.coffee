'use strict'

angular.module('nooxrApp')
  .controller 'MainCtrl', ($scope, userService, categories, ads,loadcontrolService,$route,$location) ->
    loadcontrolService.endLoad()

    $scope.pActualPage = $route.current.params.page || 1
    $scope.pNumPages = ads.pages
    $scope.pTotalItems = ads.result_count
    $scope.pItemsPerPage = $route.current.params.page_limit || 15
    $scope.pPageChange = (page)->
      _search = {'page':page}
      if $route.current.params.page_limit
        _search.page_limit = $scope.pItemsPerPage
      if $route.current.params.console_id
        _search.console_id = $route.current.params.console_id

      $location.search(_search).path("#{$location.$$path}")

    $scope.categories = categories
    $scope.games = ads.results
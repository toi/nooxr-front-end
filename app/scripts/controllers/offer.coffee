'use strict'

angular.module('nooxrApp')
  .controller 'OfferCtrl', ($scope,$location,ad,adDeals,adService,loadcontrolService,logedUser,globalService) ->
    loadcontrolService.endLoad()
    if ad.error == true || ad.ad.active == false
      return $location.path('/') 

    $scope.logedUser = logedUser
    $scope.ad = ad.ad
    $scope.similar_cosole = ad.other_ads
    console.log $scope.similar_cosole
    $scope.deals = adDeals

    $scope.denunciate = ()->
      globalService.sendMsg({
          title:'Reportar Anuncio'
          msg:'Cuentanos por qué deseas denunciar o reportar este anuncio:'
          btnLabel:'Reportar'
          subject:'Denuncia Add #'+ad.ad.id
        })

    $scope.addDeal = ()->
      if !$scope.dealText
        $scope.errorSendDeal = true

      else
        $scope.sendingDeal = true
        $scope.errorSendDeal = false
        adService.createDeal($scope.ad.id, {
          text: $scope.dealText
        }).then (response)->
          $scope.sendingDeal = false
          if !response.error
            $scope.dealText = ''
            $scope.deals = response

    $scope.buyDeal = ()->
      if $scope.ad.auto_accept
        _msg = " Esta publicación acepta compras automaticas, lo que quiere decir que al hacer click en \"Comprar\" automaticamente serías el comprador por el precio establecido por el vendedor. Si no estas seguro de la compra aun te recomendamos negociar con el vendedor a través del modulo de negociaciones y esperar que el vendedor eliga tu oferta."
      else
        _msg = " Esta publicación NO acepta compras automaticas, lo que quiere decir que al hacer click en \"Comprar\" se genera una oferta de compra por el precio establecido por el vendedor sin embargo el vendedor puede elegir si desea o no venderte este artículo. "

      globalService.showConfirmation({
          title:"Comprar #{$scope.ad.game.name}"
          msg:_msg
          btnLabel:'Comprar'
        },()->
          loadcontrolService.startLoad()
          adService.buy($scope.ad.id).then (response)->
            loadcontrolService.endLoad()
            if !response.error
              # if $scope.ad.auto_accept == true
              #   $scope.ad.finished = true
              $scope.dealText = ''
              $scope.deals = response
              window.location.reload()
        )
    
    $scope.linkFb = ->
      console.log location.href
      FB.ui
        method: "feed"
        name: $scope.ad.game.name
        link: location.href
        picture: $scope.ad.game.thumbnail
        caption: '₡'+$scope.ad.price
        description: $scope.ad.comment
        message: "Nooxr.com"

      return  

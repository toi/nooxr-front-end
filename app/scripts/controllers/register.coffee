'use strict'

angular.module('nooxrApp')
  .controller 'RegisterCtrl', ($scope,Facebook,country,userService,$location,loadcontrolService,globalService) ->
    loadcontrolService.endLoad()
    $scope.country = country
    
    

    $scope.user = {}
    $scope.user.state_id = 0
    
    $scope.facebookLogged = false
    $scope.user.country = country.name
    
    $scope.stateSelected = ()->
      $scope.user.state_id = $scope.user.state.state_id

    $scope.facebookLogin = ()->
      Facebook.login (response)->
        if response.status == 'connected'
          $scope.me()
      ,{scope: 'email'}

    $scope.me = ()->
      Facebook.api '/me', (response)->
        $scope.user = response
        $scope.user.picture = "https://graph.facebook.com/#{$scope.user.id}/picture?width=250&height=250"
        $scope.user.facebook = $scope.user.id
        $scope.facebookLogged = true
        
        userService.loginUserWithKey($scope.user.email,$scope.user.id).then (data)->
          if !data.error
            userService.setNewToken(data.token)
            $location.path('/account')

    checkExpresion = (expresion,data)->
      res = expresion.test(data)

    getValidation = ()->
      validation = {}
      validation.ok = true

      if !$scope.user.username
        validation.ok = false
        validation.username = true
        validation.emailReason = "blank"

      if $scope.user.username
        userService.checkAvailability(username:$scope.user.username).then (data)->
          if data.error == true
            validation.ok = false
            validation.username = true
            validation.usernameReason = "inuse"

      if !checkExpresion(/^([a-zA-Z0-9]{1}[\.\_]{0,}){3,}$/,$scope.user.username)
        validation.ok = false
        validation.username = true
        validation.usernameReason = "invalid"

      if checkExpresion(/(((admin){1,})|(([n]+[u|o]+[x]+[e]*[r]+){1,})){1,}/,$scope.user.username)
        validation.ok = false
        validation.username = true
        validation.usernameReason = "notallow"

      if !$scope.user.password || $scope.user.password != $scope.user.passConfirm
        validation.ok = false
        validation.password = true

      if !$scope.user.passConfirm
        validation.ok = false
        validation.passConfirm = true

      if !checkExpresion(/^([a-zA-Z0-9]{1}[\-\_\.]{0,1}){1,}@[a-zA-Z0-9]{2,}([\.]{1}[a-zA-z]{2,}){1,}$/,$scope.user.email)
        validation.ok = false
        validation.email = true
        validation.emailReason = "invalid"

      if $scope.user.email
        userService.checkAvailability(email:$scope.user.email).then (data)->
          if data.error == true
            validation.ok = false
            validation.email = true
            validation.emailReason = "inuse"

      if !$scope.user.email
        validation.ok = false
        validation.email = true
        validation.emailReason = "blank"

      if !$scope.user.phone
        validation.ok = false
        validation.phone = true

      if !$scope.user.state_id 
        validation.ok = false
        validation.state_id = true


      return validation

    $scope.registerUser = ()->
      $scope.validation = getValidation()
      if $scope.validation.ok
        $scope.disabledBtn = true
        userService.createUser($scope.user).then  (data)->
          userService.loginUser($scope.user.email,$scope.user.password).then (data)->
            if !data.error
              $scope.disabledBtn = false
              userService.setNewToken(data.token)
              goTo = if localStorage.afterLogin then localStorage.afterLogin else '/notifications'
              localStorage.afterLogin = ''
              userService.isUserLoged ()->
                $location.path(goTo)

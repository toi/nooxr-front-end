'use strict'

angular.module('nooxrApp')
  .controller 'AccountCtrl', ($scope,loadcontrolService,logedUser,country, $timeout, userService, Restangular,$rootScope, globalService) ->
    $scope.country = country
    $scope.user = angular.copy(logedUser)
    $scope.user.state_id = logedUser.state.state_id
    loadcontrolService.endLoad()

    $scope.updateUser = ()->
      if $scope.pass != '' && $scope.pass == $scope.pass_c
        $scope.user.pass = $scope.pass
        $scope.passInvalid = false
      else if $scope.pass != ''
        $scope.passInvalid = true
        $scope.user.pass = ''
      else
        $scope.passInvalid = false
        $scope.user.pass = ''
      $scope.user.state_id = $scope.user.state.state_id
      userService.updateActiveUserData($scope.user).then (user_data)->
        swal({   title: "Listo!",   text: "Tus datos se han guardado",   type: "success",   confirmButtonText: "OK" });
        $rootScope.activeUser = user_data
        $scope.user = user_data
        $scope.user.state_id = user_data.state.state_id
        if $scope.pass && !$scope.passInvalid
          $rootScope.logOut()

    $scope.progress = "0";
    $scope.onProgress = false
    $scope.onFinishUpload = false
    $scope.uploader =
      active : false
      config : 
        url         : "#{Restangular.configuration.baseUrl}/user/upload"
        fieldName   : 'file'
      extraFields :
        token       : localStorage.token
      callbacks :
        setProgress : (val)->
          $scope.onProgress = true
          $scope.onFinishUpload = false
          $scope.progress = Math.ceil(val*100)
          $scope.$apply()
        onError     : (event, name, error)->
            swal({   title: "Ops!",   text: "Hubo un error al subir tu foto de perfil",   type: "error",   confirmButtonText: "OK" });
        onFinish    : (event, total)->
          swal({   title: "Listo!",   text: "Se ha cambiado tu foto de perfil",   type: "success",   confirmButtonText: "OK" });
        onFinishOne : (event, response, name, number, total)->
          response = JSON.parse(response)
          $scope.response = response
          $scope.user.picture = $scope.response.picture
          console.log "finish",$scope.response
          $timeout () ->
            $scope.onProgress = false
            $scope.onFinishUpload = true
            $scope.$apply()
          $timeout () ->
            $scope.onFinishUpload = false
            $scope.$apply()
          ,1000
                
'use strict'

angular.module('nooxrApp')
  .controller 'PasschangeCtrl', ($scope,loadcontrolService,$route,userService) ->
    loadcontrolService.endLoad()

    $scope.sendPassChangeRequest = ()->
      $scope.validation = {}
      if $scope.pass == $scope.c_pass && $scope.pass
        $scope.validation.c_pass = false
        $scope.validation.pass = false
        userService.confirmPasswordChange($scope.pass,$route.current.params.recovery_code).then (data)->
          if "#{data.error}" == 'true'
            $scope.error = true
          else
            $scope.error = false
          $scope.result = true
      else
        $scope.validation.c_pass = true
        $scope.validation.pass = true
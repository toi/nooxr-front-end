'use strict'

angular.module('nooxrApp')
  .controller 'TradeinCtrl', ($scope,loadcontrolService,validationService,tradeinService,logedUser,globalService,$location) ->
    loadcontrolService.endLoad()

    if !logedUser.confirmed
      globalService.showAlert {title:'Usuario no confirmado',msg:'Tienes que confirmar tu usuario para publicar anuncios'}
      return $location.path('/') 

    $scope.logedUser = logedUser
    $scope.tradeIn = {}
    $scope.actualStep = 1
    _gameData = []
    _dollarPrice = 550
    gameStates = [
      {
        state:'Sin Caja'
        priceDiference:-4000
      }
      {
        state:'Regular'
        priceDiference:-2000
      }
      {
        state:'Como Nuevo'
        priceDiference:0
      }
      {
        state:'Nuevo'
        priceDiference:0
      }
    ]

    $scope.tradeIn.with_pass = false

    $scope.togglePass = ()->
      $scope.tradeIn.with_pass = if !$scope.tradeIn.with_pass then true else false

    $scope.$watch 'tradeIn.state', (newV,oldV)->
      if newV != undefined
        $scope.state = gameStates[newV]
        $scope.tradeIn.state_label = $scope.state.state
        if newV > 2
          $scope.tradeIn.price = parseInt((parseInt(_gameData['new-price'])/100)*_dollarPrice)
        else
          $scope.tradeIn.price = parseInt((parseInt(_gameData['loose-price'])/100)*_dollarPrice) + $scope.state.priceDiference
        if $scope.tradeIn.price < 0
          $scope.tradeIn.price = 0

    $scope.searchCodeBar = ()->
      $scope.validation = validationService.validate $scope.tradeIn, ['barcode']
      if $scope.validation.ok
        tradeinService.getRemoteUpc($scope.tradeIn.barcode).then (data)->
          $scope.actualStep = 2
          _gameData = data
          $scope.tradeIn.name = data['product-name']
          $scope.tradeIn.console = data['console-name']
          $scope.state = gameStates[2]
          $scope.tradeIn.state_label = $scope.state.state
          $scope.tradeIn.price = parseInt((parseInt(data['loose-price'])/100)*_dollarPrice) + $scope.state.priceDiference

    $scope.createTrade = ()->
      if $scope.tradeIn.price > 0
        tradeinService.createTrade($scope.tradeIn).then (data)->
          $scope.tradeInData = data
          $scope.actualStep = 3

    $scope.resetTrade = ()->
      $scope.tradeIn = {}
      $scope.actualStep = 1
      _gameData = []
      #temporaly fix to remove the class
      $('#tempFixCheckbox_passCode').removeClass 'checked'
      return

      



    


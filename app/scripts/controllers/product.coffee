'use strict'

angular.module('nooxrApp')
  .controller 'ProductCtrl', ($scope, product, gameService,loadcontrolService, ads, $location, $route, gameRankingService) ->
  
    if product.error == true
      loadcontrolService.endLoad()
      return $location.path('/')

    $scope.product = product.game
    $scope.ads = ads.results


    if $scope.ads.length > 0
      $scope.productTab = 1
    else
      $scope.productTab = 2


    
    
    # gameService.getCoverFromYoutube(product.game.title).success (data)->
    #   console.log('video', data)
    #   videoID = data.feed.entry[0].id.$t
    #   loadcontrolService.endLoad()
    videoID = $scope.product.video.split 'watch?v='
    videoID = videoID[1]
    $scope.image_cover = "http://img.youtube.com/vi/#{videoID}/hqdefault.jpg"

    gameService.trackGameView(product.game.id)

    $scope.gameRankingLoading = true
    gameRankingService.getRanking(product.game.title).success (data)->
      if(data[0])
        $scope.gameRankingLoading = false
        $scope.gameExternalInfo = true
        $scope.gameRanking = data[0]
        $scope.gameRanking.date = $scope.gameRanking.short_description.split(' -')
        $scope.gameRanking.date = $scope.gameRanking.date[0]
      else
        $scope.gameExternalInfo = false
        $scope.gameRankingLoading = false

      loadcontrolService.endLoad()
    .error ()->
      $scope.gameRankingLoading = false
      
    
    $scope.category = ""
    angular.forEach $scope.product.categories, (category,$i)->
      separator = if $i > 0 then "/" else ""
      $scope.category = $scope.category + " #{separator} #{category.name}" 

    $scope.chepItem = _.min $scope.ads, (chr)-> 
      return chr.price

    $scope.otherGames = product.other_games

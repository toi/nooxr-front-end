'use strict'

angular.module('nooxrApp')
  .controller 'WishlistCtrl', ($scope,wishlist,Restangular,loadcontrolService,wishlistService,$timeout) ->
    loadcontrolService.endLoad()
    $scope.wishlist = wishlist

    formatSelection = (result)->
      "<div class='big-search-result'><img src='#{result.thumbnail}'/><span>#{result.title}</span></div>"


    $scope.selectGame = {
      maximumSelectionSize : 1
      minimumInputLength: 3
      formatResult: formatSelection
      formatSelection: formatSelection
      initSelection: (element, callback)-> 
      ajax:
        url: "#{Restangular.configuration.baseUrl}/games"
        data: (term, page) ->
          return {
            search: term
            page: page
          }
        results: (data, page) ->
          return {
            results: data.results
            more: false
          }
    }

    $scope.$watch 'addGame.id', (a,b)->
      if a?.id > 0
        addGameWithId(a.id)

    $scope.addGameWithName = ()->
      wishlistService.addGameToWishlist(null,$scope.addGame.name).then (data)->
        swal({   title: "Listo!",   text: "El juego #{$scope.addGame.name} aparacerá pronto en tu lista de deseos",   type: "success",   confirmButtonText: "OK" });
        $scope.wishlist = data
        $scope.addGame.name = ''
        $scope.nolist = false
        $timeout ()->
          $scope.flashMsg = false
        ,5000
        

    addGameWithId = (id)->
      wishlistService.addGameToWishlist(id).then (data)->
        swal({   title: "Listo!",   text: "Tu juego se a guardado en tu lista de deseos",   type: "success",   confirmButtonText: "OK" });
        $scope.wishlist = data
        $scope.addGame.id = ''

    $scope.removeGame = (wishlist_id,index)->
      swal({   title: "Estás seguro?",   text: "Se eliminará de tu lista de deseos!", type: "warning", showCancelButton: true, cancelButtonText: 'Cancelar', confirmButtonColor: "#DD6B55",   confirmButtonText: "Sí, borrar!",   closeOnConfirm: false },
        ()->
          wishlistService.removeGameFromWishList(wishlist_id).then ()->
          swal("Borrado!", "Se ha borrado de tu lista de deseos.", "success"); 
          $scope.wishlist.splice index,1
      )
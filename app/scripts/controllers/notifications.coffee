'use strict'

angular.module('nooxrApp')
  .controller 'NotificationsCtrl', ($scope,loadcontrolService,notifications,$route,$location,$timeout,userService) ->
    loadcontrolService.endLoad()

    $scope.pActualPage = $route.current.params.page || 1
    $scope.pNumPages = notifications.pages
    $scope.pTotalItems = notifications.result_count
    $scope.pItemsPerPage = $route.current.params.page_limit || 15
    $scope.pPageChange = (page)->
      $location.search('page',page).path("#{$location.$$path}")

    $scope.notifications = notifications.results

    $timeout ()->
      userService.notificatioskAsRead()
    ,2000

'use strict'

angular.module('nooxrApp')
  .controller 'TransactionsCtrl', ($scope, transactions,loadcontrolService,$route,$location) ->
    loadcontrolService.endLoad()

    $scope.pActualPage = $route.current.params.page || 1
    $scope.pNumPages = transactions.pages
    $scope.pTotalItems = transactions.result_count
    $scope.pItemsPerPage = $route.current.params.page_limit || 15
    $scope.pPageChange = (page)->
      $location.search('page',page).path("#{$location.$$path}")

    $scope.transactions = transactions.results

'use strict'

angular.module('nooxrApp')
  .controller 'ContactUsCtrl', ($scope,loadcontrolService,Restangular,globalService,logedUser) ->
    loadcontrolService.endLoad()

    console.log logedUser, 'logedUser'
    $scope.logedUser = logedUser

    $scope.msg = {}

    getValidation = (requiredFields,opts)->
      validateInputs = requiredFields || []
      opts = opts || {}

      validation = []
      validation.ok = true

      #Empty fields Validation
      angular.forEach validateInputs, (val,$i)->
        if !$scope.msg[val]
          validation[val] = true
          validation.ok = false

      return validation

    $scope.sendMsg = ()->
      $scope.validation = getValidation(['msg','subject'])
      if $scope.validation.ok && $scope.logedUser
        Restangular.all('message').post({
          msg:$scope.msg.msg
          url:'/contact-us'
          subject:$scope.msg.subject
        }).then (data)->
          globalService.showAlert {title:'Mensaje Enviado',msg:"#{$scope.logedUser.name} tu mensaje fue enviado correctamente. Pronto estaremos en contacto contigo."}
          $scope.msg = {}

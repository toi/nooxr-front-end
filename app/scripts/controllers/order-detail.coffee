'use strict'

angular.module('nooxrApp')
  .controller 'OrderDetailCtrl', ($scope,loadcontrolService,logedUser,order,$location) ->
    loadcontrolService.endLoad()
    $scope.logedUser = logedUser
    if order.error == true
      return $location.path('/') 
    $scope.order = order
    $scope.subTotal = $scope.order.order.price - $scope.order.order.user_credit
    $scope.total = $scope.subTotal + $scope.order.order.additional_cost + $scope.order.order.delivery_cost
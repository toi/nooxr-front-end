'use strict'

angular.module('nooxrApp')
  .controller 'SalesActiveCtrl', ($scope,loadcontrolService,data,adService,dealService,$modal,gameStates,consoles,globalService,$route,$location,$timeout,aditionalAd) ->
    loadcontrolService.endLoad()

    $scope.pActualPage = $route.current.params.page || 1
    $scope.pNumPages = data.pages
    $scope.pTotalItems = data.result_count
    $scope.pItemsPerPage = $route.current.params.page_limit || 10
    $scope.pPageChange = (page)->
      console.log "#{$location.$$path}?page=#{page}", 'log'
      $location.search({page:page}).path("#{$location.$$path}")

    $scope.gameStates = gameStates
    $scope.consoles = consoles
    
    $scope.dealTextarea = []
    $scope.responses = []

    # User list of games
    $scope.ads = data.results

    if aditionalAd && !aditionalAd.finished
      $scope.ads.push(aditionalAd)

    $scope.addReply = (id) ->
      $scope.dealTextarea[id] = true

    $scope.sendReply = (id)->
      $scope.dealTextarea[id] = false
      dealService.declinedDeal(id, $scope.responses[id]).then (data)->
        $scope.deals = data
      angular.forEach $scope.ads, (val,$i)->
        if val.id == $scope.activeOffer.id
          $scope.ads[$i].pending_deals = $scope.ads[$i].pending_deals-1

    $scope.cancelReply = (id) ->
      $scope.dealTextarea[id] = false

    $scope.acceptOffer = (deal) ->
      globalService.showConfirmation({
          title:'Confirmar'
          msg:"¿Estás seguro que deseas aceptar esta oferta ? <br><em>#{deal.text}</em> "
        },()->
          dealService.acceptDeal(deal.id).then (data)->
            $location.path('/sales/finished')
        )

    $scope.finalizate = (ad)->
      globalService.showConfirmation({
          title:'Finalizar Anuncio'
          msg:"¿Estás seguro que deseas finalizar este anuncio ? <br><em>#{ad.game.name}</em> "
          btnLabel:'Sí, Finalizalo'
        },()->
          _obj =
            id:ad.id
            finished:"true"
          adService.editAd(_obj).then (data)->
            if data.error
              globalService.showConfirmation({
                  title:'Error al finalizar Anuncio'
                  msg:"No se pueden finalizar anuncios que no han sido publicados aún"
                  btnLabel:'Ok'
                },()->)
            else
              $location.path('/sales/finished')
        )

    $scope.autoAcceptBuy = (ad)->
      _flag = if ad.auto_accept then false else true    
      _obj =
        id:ad.id
        auto_accept:"#{_flag}"
      adService.editAd(_obj).then (data)->
        angular.forEach $scope.ads, (val,$i)->
          if ad.id == val.id
            $scope.ads[$i].auto_accept = _flag

    $scope.viewOffers = (id) ->
      $scope.activeOffer = _.where($scope.ads, {id:id})
      $scope.activeOffer = $scope.activeOffer[0]
      if $scope.activeOffer
        adService.getAdDeals(id).then (data)->
          $scope.offersOpen = true
          $scope.deals = data
      else
        $location.path('ad/'+id)

    if $route.current.params.open_ad
      $scope.viewOffers(parseInt($route.current.params.open_ad))

    $scope.closeOffers = () ->
      $scope.activeOffer = {}
      $scope.offersOpen = false


    $scope.openEditAd = (id)->
      _adToEdit = _.where($scope.ads, {id:id})
      $scope.editAd = angular.copy(_adToEdit[0])
      $scope.validation = {}
      logOutModal = $modal.open({
        templateUrl:'editAdModal'
        scope:$scope
        controller: ($scope, $modalInstance, adService)->
          $scope.cancel = ()->
            $modalInstance.close()
          $scope.save = ()->
            if $scope.editAd.price > 0
              $scope.validation.price = false
              _obj = {
                id: $scope.editAd.id
                price: $scope.editAd.price
                console_id: $scope.editAd.console.id
                game_state_id: $scope.editAd.game_state.id
                comment:$scope.editAd.comment
                accepted_games:$scope.editAd.accepted_games
              }
              adService.editAd(_obj).then (data)->
                angular.forEach $scope.ads, (val,$i)->
                  if val.id == data.id
                    $scope.ads[$i] = data
                $modalInstance.close()
            else
              $scope.validation.price = true
      })

           
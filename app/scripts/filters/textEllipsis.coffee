'use strict'

angular.module('nooxrApp')
  .filter 'textEllipsis', () ->
    (input,limit) ->
      limit = limit || 100
      if "#{input}".length > limit
        crop = "#{input}".substring(0,limit)
        return "#{crop}..."
      else
        console.log input
        return "#{input}"

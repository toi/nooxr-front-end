'use strict'

angular.module('nooxrApp')
  .filter 'consoleRoute', () ->
    (input) ->
      if input.indexOf('games') > 0
        return input
      else
        return '/games'
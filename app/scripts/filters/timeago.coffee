'use strict'

angular.module('nooxrApp')
  .filter 'timeAgo', () ->
    (dt) ->
      if not dt then return ''
      agofn = (val,unit)-> return 'hace ' + val + ' '+unit+(if val>0 then 's')
      msecs = (new Date()).getTime()-(new Date(dt)).getTime() 
      days = parseInt(msecs/(24*60*60*1000))
      if days>0 then return agofn days, 'dia'
      hours = parseInt(msecs/(60*60*1000))
      if hours>0 then return agofn hours, 'hora'
      mins = parseInt(msecs/(60*1000))  
      if mins>0 then return agofn mins, 'minuto'
      secs = parseInt(msecs/1000)
      if secs == 0 then return ''
      return agofn secs,'segundo'
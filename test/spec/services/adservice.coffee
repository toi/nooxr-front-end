'use strict'

describe 'Service: adService', () ->

  # load the service's module
  beforeEach module 'nooxrApp'

  # instantiate service
  adService = {}
  beforeEach inject (_adService_) ->
    adService = _adService_

  it 'should do something', () ->
    expect(!!adService).toBe true

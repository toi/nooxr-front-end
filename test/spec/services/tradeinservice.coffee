'use strict'

describe 'Service: tradeinService', () ->

  # load the service's module
  beforeEach module 'nooxrApp'

  # instantiate service
  tradeinService = {}
  beforeEach inject (_tradeinService_) ->
    tradeinService = _tradeinService_

  it 'should do something', () ->
    expect(!!tradeinService).toBe true

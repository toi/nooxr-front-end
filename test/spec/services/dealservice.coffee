'use strict'

describe 'Service: dealService', () ->

  # load the service's module
  beforeEach module 'nooxrApp'

  # instantiate service
  dealService = {}
  beforeEach inject (_dealService_) ->
    dealService = _dealService_

  it 'should do something', () ->
    expect(!!dealService).toBe true

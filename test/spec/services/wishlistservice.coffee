'use strict'

describe 'Service: Wishlistservice', () ->

  # load the service's module
  beforeEach module 'nooxrApp'

  # instantiate service
  Wishlistservice = {}
  beforeEach inject (_Wishlistservice_) ->
    Wishlistservice = _Wishlistservice_

  it 'should do something', () ->
    expect(!!Wishlistservice).toBe true

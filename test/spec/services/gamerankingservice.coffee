'use strict'

describe 'Service: gameRankingService', ->

  # load the service's module
  beforeEach module 'nooxrApp'

  # instantiate service
  gameRankingService = {}
  beforeEach inject (_gameRankingService_) ->
    gameRankingService = _gameRankingService_

  it 'should do something', ->
    expect(!!gameRankingService).toBe true

'use strict'

describe 'Service: deliveryService', () ->

  # load the service's module
  beforeEach module 'nooxrApp'

  # instantiate service
  deliveryService = {}
  beforeEach inject (_deliveryService_) ->
    deliveryService = _deliveryService_

  it 'should do something', () ->
    expect(!!deliveryService).toBe true

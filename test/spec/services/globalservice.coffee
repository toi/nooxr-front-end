'use strict'

describe 'Service: globalService', () ->

  # load the service's module
  beforeEach module 'nooxrApp'

  # instantiate service
  globalService = {}
  beforeEach inject (_globalService_) ->
    globalService = _globalService_

  it 'should do something', () ->
    expect(!!globalService).toBe true

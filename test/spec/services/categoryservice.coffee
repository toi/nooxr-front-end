'use strict'

describe 'Service: categoryService', () ->

  # load the service's module
  beforeEach module 'nooxrApp'

  # instantiate service
  categoryService = {}
  beforeEach inject (_categoryService_) ->
    categoryService = _categoryService_

  it 'should do something', () ->
    expect(!!categoryService).toBe true

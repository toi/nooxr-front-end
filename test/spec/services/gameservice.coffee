'use strict'

describe 'Service: gameService', () ->

  # load the service's module
  beforeEach module 'nooxrApp'

  # instantiate service
  gameService = {}
  beforeEach inject (_gameService_) ->
    gameService = _gameService_

  it 'should do something', () ->
    expect(!!gameService).toBe true

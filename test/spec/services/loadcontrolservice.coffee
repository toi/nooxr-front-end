'use strict'

describe 'Service: Loadcontrolservice', () ->

  # load the service's module
  beforeEach module 'nooxrApp'

  # instantiate service
  Loadcontrolservice = {}
  beforeEach inject (_Loadcontrolservice_) ->
    Loadcontrolservice = _Loadcontrolservice_

  it 'should do something', () ->
    expect(!!Loadcontrolservice).toBe true

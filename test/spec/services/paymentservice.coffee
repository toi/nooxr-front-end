'use strict'

describe 'Service: paymentService', () ->

  # load the service's module
  beforeEach module 'nooxrApp'

  # instantiate service
  paymentService = {}
  beforeEach inject (_paymentService_) ->
    paymentService = _paymentService_

  it 'should do something', () ->
    expect(!!paymentService).toBe true

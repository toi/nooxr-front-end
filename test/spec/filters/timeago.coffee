'use strict'

describe 'Filter: timeAgo', () ->

  # load the filter's module
  beforeEach module 'nooxrApp'

  # initialize a new instance of the filter before each test
  timeAgo = {}
  beforeEach inject ($filter) ->
    timeAgo = $filter 'timeAgo'

  it 'should return the input prefixed with "timeAgo filter:"', () ->
    text = 'angularjs'
    expect(timeAgo text).toBe ('timeAgo filter: ' + text)

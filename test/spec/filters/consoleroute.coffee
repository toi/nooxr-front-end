'use strict'

describe 'Filter: consoleRoute', () ->

  # load the filter's module
  beforeEach module 'nooxrApp'

  # initialize a new instance of the filter before each test
  consoleRoute = {}
  beforeEach inject ($filter) ->
    consoleRoute = $filter 'consoleRoute'

  it 'should return the input prefixed with "consoleRoute filter:"', () ->
    text = 'angularjs'
    expect(consoleRoute text).toBe ('consoleRoute filter: ' + text)

'use strict'

describe 'Filter: take', () ->

  # load the filter's module
  beforeEach module 'nooxrApp'

  # initialize a new instance of the filter before each test
  take = {}
  beforeEach inject ($filter) ->
    take = $filter 'take'

  it 'should return the input prefixed with "take filter:"', () ->
    text = 'angularjs'
    expect(take text).toBe ('take filter: ' + text)

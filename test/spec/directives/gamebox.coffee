'use strict'

describe 'Directive: gameBox', () ->

  # load the directive's module
  beforeEach module 'nooxrApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<game-box></game-box>'
    element = $compile(element) scope
    expect(element.text()).toBe 'this is the gameBox directive'

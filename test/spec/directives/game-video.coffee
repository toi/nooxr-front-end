'use strict'

describe 'Directive: gameVideo', () ->

  # load the directive's module
  beforeEach module 'nooxrApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<game-video></game-video>'
    element = $compile(element) scope
    expect(element.text()).toBe 'this is the gameVideo directive'

'use strict'

describe 'Directive: currencyInput', () ->

  # load the directive's module
  beforeEach module 'nooxrApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<currency-input></currency-input>'
    element = $compile(element) scope
    expect(element.text()).toBe 'this is the currencyInput directive'

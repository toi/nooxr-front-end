'use strict'

describe 'Directive: tagsinput', ->

  # load the directive's module
  beforeEach module 'nooxrApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<tagsinput></tagsinput>'
    element = $compile(element) scope
    expect(element.text()).toBe 'this is the tagsinput directive'

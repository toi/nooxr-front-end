'use strict'

describe 'Controller: AddAdCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  AddAdCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    AddAdCtrl = $controller 'AddAdCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

'use strict'

describe 'Controller: OrderDetailCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  OrderDetailCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    OrderDetailCtrl = $controller 'OrderDetailCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

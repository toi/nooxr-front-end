'use strict'

describe 'Controller: AccountCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  AccountCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    AccountCtrl = $controller 'AccountCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

'use strict'

describe 'Controller: SalesFinishedCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  SalesFinishedCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    SalesFinishedCtrl = $controller 'SalesFinishedCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

'use strict'

describe 'Controller: PaymentCompletedCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  PaymentCompletedCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    PaymentCompletedCtrl = $controller 'PaymentCompletedCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

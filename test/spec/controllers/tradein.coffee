'use strict'

describe 'Controller: TradeinCtrl', ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  TradeinCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    TradeinCtrl = $controller 'TradeinCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3

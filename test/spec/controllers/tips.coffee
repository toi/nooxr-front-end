'use strict'

describe 'Controller: TipsCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  TipsCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    TipsCtrl = $controller 'TipsCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

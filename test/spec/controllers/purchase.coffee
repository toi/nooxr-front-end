'use strict'

describe 'Controller: PurchaseCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  PurchaseCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    PurchaseCtrl = $controller 'PurchaseCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

'use strict'

describe 'Controller: AdCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  AdCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    AdCtrl = $controller 'AdCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

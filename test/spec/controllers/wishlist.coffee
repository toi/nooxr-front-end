'use strict'

describe 'Controller: WishlistCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  WishlistCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    WishlistCtrl = $controller 'WishlistCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

'use strict'

describe 'Controller: LimitationsCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  LimitationsCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    LimitationsCtrl = $controller 'LimitationsCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

'use strict'

describe 'Controller: ContactUsCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  ContactUsCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ContactUsCtrl = $controller 'ContactUsCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

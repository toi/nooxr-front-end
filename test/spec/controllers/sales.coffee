'use strict'

describe 'Controller: SalesCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  SalesCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    SalesCtrl = $controller 'SalesCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

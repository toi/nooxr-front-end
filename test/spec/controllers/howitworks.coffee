'use strict'

describe 'Controller: HowitworksCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  HowitworksCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    HowitworksCtrl = $controller 'HowitworksCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

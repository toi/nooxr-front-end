'use strict'

describe 'Controller: TradeinsCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  TradeinsCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    TradeinsCtrl = $controller 'TradeinsCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

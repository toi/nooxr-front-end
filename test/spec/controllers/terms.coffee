'use strict'

describe 'Controller: TermsCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  TermsCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    TermsCtrl = $controller 'TermsCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

'use strict'

describe 'Controller: TransactionsCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  TransactionsCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    TransactionsCtrl = $controller 'TransactionsCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

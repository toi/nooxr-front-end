'use strict'

describe 'Controller: ConfirmCtrl', () ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  ConfirmCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ConfirmCtrl = $controller 'ConfirmCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', () ->
    expect(scope.awesomeThings.length).toBe 3

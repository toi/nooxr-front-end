'use strict'

describe 'Controller: PaymentCtrl', ->

  # load the controller's module
  beforeEach module 'nooxrApp'

  PaymentCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    PaymentCtrl = $controller 'PaymentCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
